const express = require('express')
const usersRouter = require('./routes/users')
const blogRouter = require('./routes/blogs')

const app = express()


app.use(express.json())

app.use('/users',usersRouter)
app.use('/blogs',blogRouter)

app.listen(4000, '0.0.0.0', () => {
    console.log(`server started successfully on port 4000`)
  })